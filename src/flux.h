/* Copyright (c) 2014 Moritz Bitsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef libflux_flux_h
#define libflux_flux_h

#include "vectr.h"

typedef struct _flx_dispatcher flx_dispatcher;
typedef struct _flx_store flx_store;

struct _flx_dispatcher {
	int idle;
	VECTR_T(flx_store *) strores;
};

struct _flx_store {
	int idle;
	int (*callback)(flx_store *self, flx_dispatcher *origin, int action, void *data);
};

int flx_dispatcher_register_store(flx_dispatcher *slf, flx_store *store);
int flx_dispatcher_dispatch(flx_dispatcher *slf, int action, void *data);
int flx_action_new_id();
int flx_store_wait_for(flx_dispatcher *dispatcher, ...);

#endif
