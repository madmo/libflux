/* Copyright (c) 2014 Moritz Bitsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "flux.h"
#include <stdarg.h>

/**
 *  Register a dispatcher with a store
 *
 *  @param slf pointer to dispatcher (self)
 *  @param store store to register
 *
 *  @return returns store id in current dispatcher
 */
int flx_dispatcher_register_store(flx_dispatcher *slf, flx_store *store)
{
	if (!slf->idle)
		return -1;

	VECTR_Push(flx_store *, slf->strores, store);

	return (int)VECTR_Len(slf->strores) - 1;
}

/**
 *  Dispatch action on selected dispatcher
 *
 *  @param slf pointer to dispatcher (self)
 *  @param action to dispatch
 *  @param data action data
 *
 *  @return returns 0
 */
int flx_dispatcher_dispatch(flx_dispatcher *slf, int action, void *data)
{
	int i;
	int rest;

	if (!slf->idle)
		return -1;

	rest = (int)VECTR_Len(slf->strores);

	/* Mark all as busy */
	slf->idle = 0;
	for (i = 0; i < VECTR_Len(slf->strores); i++)
		VECTR_A(slf->strores, i)->idle = 0;

	while (rest > 0)
	{
		for (i = 0; i < VECTR_Len(slf->strores); i++)
		{
			if (VECTR_A(slf->strores, i)->callback(VECTR_A(slf->strores, i), slf, action, data))
			{
				VECTR_A(slf->strores, i)->idle = 1;
				rest--;
			}
		}
	}

	slf->idle = 1;

	return 0;
}

/**
 *  Allocate new unique action id
 *
 *  @return returns new actionid
 */
int flx_action_new_id()
{
	static int _id_max = 0;

	return ++_id_max;
}

/**
 *  Wait for stores to be idle
 *
 *  A store is idle if it has been updated within the current dispatcher iteration
 *
 *  @param dispatcher to check stores in
 *  @param ... id's of stores (negative value terminates list)
 *
 *  @return 0 if all selected stores are idle, -1 on error
 */
int flx_store_wait_for(flx_dispatcher *dispatcher, ...)
{
	va_list ap;
	int arg;
	int res = 0;

	if (dispatcher->idle)
		return -1;

	va_start(ap, dispatcher);

	while ((arg = va_arg(ap, int)) >= 0)
	{
		if (arg >= VECTR_Len(dispatcher->strores))
		{
			res = -1;
			break;
		}

		if (!VECTR_A(dispatcher->strores, arg)->idle)
		{
			res = 1;
			break;
		}
	}

	va_end(ap);

	return res;
}
