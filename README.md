# README #

A naive implementation of a single direction data flow library

Inspired by flux (http://facebook.github.io/react/docs/flux-overview.html)

### How do I get set up? ###

1. clone this repository
2. build with Xcode or cmake
3. enjoy